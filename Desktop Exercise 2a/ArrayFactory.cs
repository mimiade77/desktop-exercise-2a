﻿using System;
using System.Linq;

namespace Desktop_Exercise_2a
{
  public static class ArrayFactory
  {
    /// <summary>
    /// random number generator
    /// </summary>
    private static readonly Random random = new Random();

    /// <summary>
    /// returns an array of random decimals
    /// </summary>
    /// <param name="size"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    public static decimal[] GetArray(int size, decimal min, decimal max)
    {
      var arr = new decimal[size];

      for (int i = 0; i < arr.Length; i++)
      {
        arr[i] = RandomNumberBetween(min, max);
      }

      return arr;
          }

    /// <summary>
    /// Output the contents of the array in the current order.
    /// </summary>
    /// <param name="arr"></param>
    public static void OutputArray(decimal[] arr)
    {
      foreach(var i in arr)
      {
        Console.WriteLine(string.Format("{0:C}", i));
      }
    }

    /// <summary>
    /// Find the average value of the items in the array.
    /// </summary>
    /// <param name="arr"></param>
    /// <returns></returns>
    public static decimal AverageArrayValue(decimal[] arr)
    {
      return arr.Average();
    }

    /// <summary>
    /// Find the item with the higest value in the array.
    /// </summary>
    /// <param name="arr"></param>
    /// <returns></returns>
    public static decimal MaxArrayValue(decimal[] arr)
    {
      return arr.Max();
    }

    /// <summary>
    /// Find the item with the lowest value in the array.
    /// </summary>
    /// <param name="arr"></param>
    /// <returns></returns>
    public static decimal MinArrayValue(decimal[] arr)
    {
      return arr.Min();
    }

    /// <summary>
    /// Sort the array so the contents are in ascending order.
    /// </summary>
    /// <param name="arr"></param>
    public static void SortArrayAsc(decimal[] arr)
    {
      Array.Sort(arr);
    }

    public static void SortArrayDesc(decimal[] arr)
    {
      Array.Sort(arr);
      Array.Reverse(arr);
    }

    /// <summary>
    /// generates a random decimal number between the given min and max
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    private static decimal RandomNumberBetween(decimal min, decimal max)
    {
      var next = (decimal)random.NextDouble();

      return min + (next * (max - min));
    }
  }
}
